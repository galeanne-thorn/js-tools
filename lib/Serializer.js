/**
 * Module GT-Tools
 *
 * Copyright © 2015 Galeanne Thorn
 * License: MIT
 */
var Serializer = (function () {
    function Serializer() {
    }
    Serializer.serializeValue = function (value) {
        var type = typeof value;
        if (type === "undefined") {
            value = null;
        }
        else if (type === "object" || type === "number") {
            value = JSON.stringify(value);
        }
        return value;
    };
    Serializer.deserializeValue = function (value) {
        if (!value || value === 'null') {
            return null;
        }
        if (value.charAt(0) === "{" || value.charAt(0) === "[" || Serializer.isStringNumber(value)) {
            return JSON.parse(value);
        }
        return value;
    };
    Serializer.isStringNumber = function (num) {
        return /^-?\d+\.?\d*$/.test(num.replace(/["']/g, ''));
    };
    return Serializer;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Serializer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VyaWFsaXplci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9TZXJpYWxpemVyLnRzIl0sIm5hbWVzIjpbIlNlcmlhbGl6ZXIiLCJTZXJpYWxpemVyLmNvbnN0cnVjdG9yIiwiU2VyaWFsaXplci5zZXJpYWxpemVWYWx1ZSIsIlNlcmlhbGl6ZXIuZGVzZXJpYWxpemVWYWx1ZSIsIlNlcmlhbGl6ZXIuaXNTdHJpbmdOdW1iZXIiXSwibWFwcGluZ3MiOiJBQUFBOzs7OztHQUtHO0FBRUg7SUFBQUE7SUFrQ0FDLENBQUNBO0lBL0JpQkQseUJBQWNBLEdBQTVCQSxVQUE2QkEsS0FBVUE7UUFDbkNFLElBQUlBLElBQUlBLEdBQUdBLE9BQU9BLEtBQUtBLENBQUNBO1FBRXhCQSxFQUFFQSxDQUFDQSxDQUFDQSxJQUFJQSxLQUFLQSxXQUFXQSxDQUFDQSxDQUFDQSxDQUFDQTtZQUN2QkEsS0FBS0EsR0FBR0EsSUFBSUEsQ0FBQ0E7UUFDakJBLENBQUNBO1FBQ0RBLElBQUlBLENBQUNBLEVBQUVBLENBQUNBLENBQUNBLElBQUlBLEtBQUtBLFFBQVFBLElBQUlBLElBQUlBLEtBQUtBLFFBQVFBLENBQUNBLENBQUNBLENBQUNBO1lBQzlDQSxLQUFLQSxHQUFHQSxJQUFJQSxDQUFDQSxTQUFTQSxDQUFDQSxLQUFLQSxDQUFDQSxDQUFDQTtRQUNsQ0EsQ0FBQ0E7UUFDREEsTUFBTUEsQ0FBQ0EsS0FBS0EsQ0FBQ0E7SUFDakJBLENBQUNBO0lBR2FGLDJCQUFnQkEsR0FBOUJBLFVBQStCQSxLQUFhQTtRQUd4Q0csRUFBRUEsQ0FBQ0EsQ0FBQ0EsQ0FBQ0EsS0FBS0EsSUFBSUEsS0FBS0EsS0FBS0EsTUFBTUEsQ0FBQ0EsQ0FBQ0EsQ0FBQ0E7WUFDN0JBLE1BQU1BLENBQUNBLElBQUlBLENBQUNBO1FBQ2hCQSxDQUFDQTtRQUVEQSxFQUFFQSxDQUFDQSxDQUFDQSxLQUFLQSxDQUFDQSxNQUFNQSxDQUFDQSxDQUFDQSxDQUFDQSxLQUFLQSxHQUFHQSxJQUFJQSxLQUFLQSxDQUFDQSxNQUFNQSxDQUFDQSxDQUFDQSxDQUFDQSxLQUFLQSxHQUFHQSxJQUFJQSxVQUFVQSxDQUFDQSxjQUFjQSxDQUFDQSxLQUFLQSxDQUFDQSxDQUFDQSxDQUFDQSxDQUFDQTtZQUN6RkEsTUFBTUEsQ0FBQ0EsSUFBSUEsQ0FBQ0EsS0FBS0EsQ0FBQ0EsS0FBS0EsQ0FBQ0EsQ0FBQ0E7UUFDN0JBLENBQUNBO1FBRURBLE1BQU1BLENBQUNBLEtBQUtBLENBQUNBO0lBQ2pCQSxDQUFDQTtJQUdjSCx5QkFBY0EsR0FBN0JBLFVBQThCQSxHQUFXQTtRQUNyQ0ksTUFBTUEsQ0FBQ0EsZUFBZUEsQ0FBQ0EsSUFBSUEsQ0FBQ0EsR0FBR0EsQ0FBQ0EsT0FBT0EsQ0FBQ0EsT0FBT0EsRUFBRUEsRUFBRUEsQ0FBQ0EsQ0FBQ0EsQ0FBQ0E7SUFDMURBLENBQUNBO0lBQ0xKLGlCQUFDQTtBQUFEQSxDQUFDQSxBQWxDRCxJQWtDQztBQWxDRDs0QkFrQ0MsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBNb2R1bGUgR1QtVG9vbHNcclxuICpcclxuICogQ29weXJpZ2h0IMKpIDIwMTUgR2FsZWFubmUgVGhvcm5cclxuICogTGljZW5zZTogTUlUXHJcbiAqL1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2VyaWFsaXplciB7XHJcblxyXG4gICAgLy8gU2VyaWFsaXplcyB2YWx1ZSB0byBKU09OXHJcbiAgICBwdWJsaWMgc3RhdGljIHNlcmlhbGl6ZVZhbHVlKHZhbHVlOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCB0eXBlID0gdHlwZW9mIHZhbHVlO1xyXG4gICAgICAgIC8vIExldCdzIGNvbnZlcnQgdW5kZWZpbmVkIHZhbHVlcyB0byBudWxsIHRvIGdldCB0aGUgdmFsdWUgY29uc2lzdGVudFxyXG4gICAgICAgIGlmICh0eXBlID09PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAodHlwZSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlID09PSBcIm51bWJlclwiKSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gSlNPTi5zdHJpbmdpZnkodmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRGVzZXJpYWxpemVzIHZhbHVlIGZyb20gSlNPTlxyXG4gICAgcHVibGljIHN0YXRpYyBkZXNlcmlhbGl6ZVZhbHVlKHZhbHVlOiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIC8vIGFuZ3VsYXIudG9Kc29uIHdpbGwgY29udmVydCBudWxsIHRvICdudWxsJywgc28gYSBwcm9wZXIgY29udmVyc2lvbiBpcyBuZWVkZWRcclxuICAgICAgICAvLyBGSVhNRSBub3QgYSBwZXJmZWN0IHNvbHV0aW9uLCBzaW5jZSBhIHZhbGlkICdudWxsJyBzdHJpbmcgY2FuJ3QgYmUgc3RvcmVkXHJcbiAgICAgICAgaWYgKCF2YWx1ZSB8fCB2YWx1ZSA9PT0gJ251bGwnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHZhbHVlLmNoYXJBdCgwKSA9PT0gXCJ7XCIgfHwgdmFsdWUuY2hhckF0KDApID09PSBcIltcIiB8fCBTZXJpYWxpemVyLmlzU3RyaW5nTnVtYmVyKHZhbHVlKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gSlNPTi5wYXJzZSh2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHByaXZhdGUgc3RhdGljIGlzU3RyaW5nTnVtYmVyKG51bTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIC9eLT9cXGQrXFwuP1xcZCokLy50ZXN0KG51bS5yZXBsYWNlKC9bXCInXS9nLCAnJykpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==