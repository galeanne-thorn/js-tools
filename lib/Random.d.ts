export declare type RandomRange = number | [number, number];
export declare function next(range: RandomRange, max?: number): number;
export declare function fromArrayDistinct<T>(candidates: Array<T>, count: number): Array<T>;
export declare function throwDice(): number;
