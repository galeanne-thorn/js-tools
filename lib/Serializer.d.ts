export default class Serializer {
    static serializeValue(value: any): string;
    static deserializeValue(value: string): any;
    private static isStringNumber(num);
}
