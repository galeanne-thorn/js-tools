/// <reference path="../../typings/tsd.d.ts"/>
import * as DeepCopy from "../../lib/DeepCopy";

describe("DeepCopy", ()=> {

	var source = {
		"int" : 1,
		"double" : 2.34567,
		"array" : ["text", 1, 2.34567, {"a" : 1, "b" : "text"}],
		"obj" : {
			"c" : "data",
			"d" : 1
		}
	};

	it("copies object correctly", () => {
		var target : any = {};
		DeepCopy.copy(source, target);
		expect(target.int).toBe(1);
		expect(target.double).toBe(2.34567);
		expect(target.array).toEqual(source.array);
		expect(target.obj).toEqual(source.obj);
	});

	it("creates new object correctly", () => {
		var target = DeepCopy.newCopy(source);
		expect(target.int).toBe(1);
		expect(target.double).toBe(2.34567);
		expect(target.array).toEqual(source.array);
		expect(target.obj).toEqual(source.obj);
	});
});
