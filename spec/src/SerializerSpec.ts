/// <reference path="../../typings/tsd.d.ts"/>
import Serializer from "../../lib/Serializer";

describe("Serializer", () => {

	var text = "Text";
    var number = -5.4321;
	var array = [text, number];
	var object = {
		numProperty: number,
		textProperty: text
	}

	var serializedText: string;
	var serializedNumber: string;
	var serializedArray: string;
	var serializedObject: string;

	it("should serialize string as string", () => {
		serializedText = Serializer.serializeValue(text);
		expect(serializedText).toBe("Text");
	});

	it("should serialize number as string", () => {
		serializedNumber = Serializer.serializeValue(number);
		expect(serializedNumber).toBe("-5.4321");
	});

	it("should serialize array correctly", () => {
		serializedArray = Serializer.serializeValue(array);
		expect(serializedArray).toBe('["Text",-5.4321]');
	});

	it("should serialize object correctly", () => {
		serializedObject = Serializer.serializeValue(object);
		expect(serializedObject).toBe('{"numProperty":-5.4321,"textProperty":"Text"}');
	});

	it("should deserialize text correctly", () => {
		var result = Serializer.deserializeValue(serializedText);
		expect(result).toBe(text);
	});

	it("should deserialize number correctly", () => {
		var result = Serializer.deserializeValue(serializedNumber);
		expect(result).toBe(number);
	});

	it("should deserialize array correctly", () => {
		var result = Serializer.deserializeValue(serializedArray);
		expect(result).toEqual(array);
	});

	it("should deserialize object correctly", () => {
		var result = Serializer.deserializeValue(serializedObject);
		expect(result).toEqual(object);
	});
});
