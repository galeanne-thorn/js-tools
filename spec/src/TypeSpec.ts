/// <reference path="../../typings/tsd.d.ts"/>
import * as T from "../../lib/Type";

describe("isArray", () => {

	it("recognizes array as array", () => {
		var candidate = [1,2,3];
		expect(T.isArray(candidate)).toBe(true);
	});

	it("recognizes object as not array", () => {
		var candidate = {};
		expect(T.isArray(candidate)).toBe(false);
	});

	it("recognizes null as not array", () => {
		var candidate = null;
		expect(T.isArray(candidate)).toBe(false);
	});

	it("recognizes string as not array", () => {
		var candidate = "string";
		expect(T.isArray(candidate)).toBe(false);
	});

	it("recognizes number as not array", () => {
		var candidate = 5.2;
		expect(T.isArray(candidate)).toBe(false);
	});
});
