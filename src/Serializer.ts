/**
 * Module GT-Tools
 *
 * Copyright © 2015 Galeanne Thorn
 * License: MIT
 */

export default class Serializer {

    // Serializes value to JSON
    public static serializeValue(value: any): string {
        let type = typeof value;
        // Let's convert undefined values to null to get the value consistent
        if (type === "undefined") {
            value = null;
        }
        else if (type === "object" || type === "number") {
            value = JSON.stringify(value);
        }
        return value;
    }

    // Deserializes value from JSON
    public static deserializeValue(value: string): any {
        // angular.toJson will convert null to 'null', so a proper conversion is needed
        // FIXME not a perfect solution, since a valid 'null' string can't be stored
        if (!value || value === 'null') {
            return null;
        }

        if (value.charAt(0) === "{" || value.charAt(0) === "[" || Serializer.isStringNumber(value)) {
            return JSON.parse(value);
        }

        return value;
    }


    private static isStringNumber(num: string): boolean {
        return /^-?\d+\.?\d*$/.test(num.replace(/["']/g, ''));
    }
}
