﻿/**
 * Module GT-Tools
 *
 * Copyright © 2015 Galeanne Thorn
 * License: MIT
 */

import {isArray} from "./Type";

/**
 * Deep copies source to destination
 */
export function copy(source: any, destination: any): any {
    for (var property in source) {
        let sproperty = source[property];
        // Array - copy item by item
        if (isArray(sproperty)) {
            destination[property] = destination[property] || [];
            sproperty.forEach(i => destination[property].push(newCopy(i)));
        }
        // Object - copy properties
        else if (typeof sproperty === "object" && sproperty !== null) {
            destination[property] = destination[property] || {};
            copy(sproperty, destination[property]);
        } else {
            destination[property] = source[property];
        }
    }
    return destination;
};

/**
 * Creates new deep copy of source
 */
export function newCopy(source: any): any {
    // Array - copy item by item
    if (isArray(source)) {
        let destinationArray = [];
        source.forEach(i => destinationArray.push(newCopy(i)));
        return destinationArray;
    }
    // Object - do deep copy
    if (typeof source === "object") {
        let destination = {};
        return copy(source, destination);
    }
    // Value - return as is
    return source;
}
