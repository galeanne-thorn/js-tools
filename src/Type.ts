/**
 * Module GT-Tools
 *
 * Copyright © 2015 Galeanne Thorn
 * License: MIT
 */

/**
 * Determines whether arr is Array. Note that this does not work in multi DOM environment (iframes)
 */
export function isArray(arr : any) : boolean {
    return !!arr && arr.constructor == Array;
}
