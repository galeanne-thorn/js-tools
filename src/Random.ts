﻿/**
 * Module GT-Tools
 *
 * Copyright © 2015 Galeanne Thorn
 * License: MIT
 */

/**
 * Random range
 */
export type RandomRange = number |[number, number];

/**
 * Gets random whole number in interval <min; max)
 */
export function next(range: RandomRange, max?: number) {

    let min: number = 0;

    if (typeof range === 'number') {
        min = range;
        max = max || min;
    }
    else {
        min = range[0];
        max = range[1];
    }
    return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * Randomly selects count items from the candidates. Ensures no duplication in the result.
 * Note that if there is not enough candidates to select randomly, returns all candidates
 */
export function fromArrayDistinct<T>(candidates: Array<T>, count: number): Array<T> {

    if (candidates.length <= count) return candidates.map(c => { return c });

    let indexes = new Array<number>();
    for (let i = 0; i < count; ++i) {
        let newIndex = next(0, candidates.length);
        while (indexes.indexOf(newIndex) >= 0) {
            newIndex = (newIndex + 1) % candidates.length;
        }
        indexes.push(newIndex);
    }
    return indexes.map(i => { return candidates[i] });
}

/**
 * Dice throw 4dF
 * Returns random number in range -4 to 4
 * -4/+4 has chance 1:81
 * 0 has chance    19:81
 */
export function throwDice(): number {
    let sum = 0;
    for (let i = 0; i < 4; ++i) {
        sum += Math.floor(Math.random() * 3) - 1;
    }
    return sum;
}
